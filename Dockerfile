FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.6_10

LABEL author="Bladimir Villafuerte Salvador"
RUN apk add --no-cache tzdata
ENV TZ='America/Lima'
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apk --update add fontconfig ttf-dejavu

WORKDIR /

RUN mkdir app && chmod 777 app

COPY target/bcp-app-ws-tipocambio-0.0.1-SNAPSHOT /app

WORKDIR /app

EXPOSE 8080

CMD ["java","-jar","-Xmx700M","bcp-app-ws-tipocambio-0.0.1-SNAPSHOT.jar"]
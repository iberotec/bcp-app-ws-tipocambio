package pe.bcp.tipocambio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BcpAppWsTipocambioApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcpAppWsTipocambioApplication.class, args);
	}

}

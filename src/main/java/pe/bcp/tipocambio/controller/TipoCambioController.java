package pe.bcp.tipocambio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.bcp.tipocambio.entity.TipoCambio;
import pe.bcp.tipocambio.service.iface.ITipoCambioService;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/tipoCambio")
public class TipoCambioController {
	
	@Autowired
	private ITipoCambioService tipoCambioService;
	
	@PostMapping
	public Mono<TipoCambio> realizarCambio(@RequestBody TipoCambio tipoCambio){
		return tipoCambioService.realizarCambio(tipoCambio);
	}
	
}

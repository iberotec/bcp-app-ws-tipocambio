package pe.bcp.tipocambio.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import pe.bcp.tipocambio.exception.BcpException;
import pe.bcp.tipocambio.exception.ErrorDetails;

@ControllerAdvice
public class AdviceController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(BcpException.class)
	public final ResponseEntity<ErrorDetails> businessExceptionHandler(BcpException ex,
			WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false), false);
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
}

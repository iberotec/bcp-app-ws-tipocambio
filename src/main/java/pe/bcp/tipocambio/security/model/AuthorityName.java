package pe.bcp.tipocambio.security.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}

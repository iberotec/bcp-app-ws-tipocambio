package pe.bcp.tipocambio.service.iface;

import pe.bcp.tipocambio.entity.TipoCambio;
import reactor.core.publisher.Mono;

public interface ITipoCambioService {

	Mono<TipoCambio> realizarCambio(TipoCambio tipoCambio);

}

package pe.bcp.tipocambio.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.bcp.tipocambio.entity.TipoCambio;
import pe.bcp.tipocambio.entity.TipoCambioConfiguracion;
import pe.bcp.tipocambio.exception.BcpException;
import pe.bcp.tipocambio.repository.TipoCambioConfiguracionRepository;
import pe.bcp.tipocambio.repository.TipoCambioRepository;
import pe.bcp.tipocambio.service.iface.ITipoCambioService;
import reactor.core.publisher.Mono;

@Service
public class TipoCambioServiceImpl implements ITipoCambioService{
	
	@Autowired 
	private TipoCambioRepository tipoCambioRepository;
	
	@Autowired
	private TipoCambioConfiguracionRepository tipoCambioConfigRepository;

	@Override
	public Mono<TipoCambio> realizarCambio(TipoCambio tipoCambio) {
		
		TipoCambioConfiguracion config = tipoCambioConfigRepository.findByMonedaOrigenAndMonedaDestino(tipoCambio.getMonedaOrigen(), tipoCambio.getMonedaDestino());
		
		if(config == null) {
			throw new BcpException("Tipo de cambio no definido.");
		}
		
		BigDecimal montoBase = BigDecimal.valueOf(config.getMontoCambio());
		BigDecimal montoCambio = BigDecimal.valueOf(tipoCambio.getMonto());
		
		BigDecimal resultado = montoBase.multiply(montoCambio).setScale(2, RoundingMode.UP);
		
		tipoCambio.setMontoCambio(resultado.doubleValue());
		tipoCambioRepository.save(tipoCambio);
		return Mono.just(tipoCambio);
	}

}

package pe.bcp.tipocambio.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TipoCambioConfiguracion {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String monedaOrigen;
	private String monedaDestino;
	private double montoCambio;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public String getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public double getMontoCambio() {
		return montoCambio;
	}
	public void setMontoCambio(double montoCambio) {
		this.montoCambio = montoCambio;
	}
}

package pe.bcp.tipocambio.exception;

public class BcpException extends RuntimeException{
	
	public BcpException(String message) {
		super(message);
	}
	
}

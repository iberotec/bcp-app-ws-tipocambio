package pe.bcp.tipocambio.exception;

import java.util.Date;

public class ErrorDetails {
	private Date timestamp;
	private String message;
	private boolean success;
	private String details;

	public ErrorDetails(Date timestamp, String message, String details, boolean success) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
		this.success = success;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}

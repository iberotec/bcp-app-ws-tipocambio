package pe.bcp.tipocambio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.bcp.tipocambio.entity.TipoCambioConfiguracion;

@Repository
public interface TipoCambioConfiguracionRepository extends JpaRepository<TipoCambioConfiguracion, Long>{

	TipoCambioConfiguracion findByMonedaOrigenAndMonedaDestino(String monedaOrigen,String monedaDestino);

}
